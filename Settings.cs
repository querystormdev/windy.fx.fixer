using Jot.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using QueryStorm.Apps;
using QueryStorm.Data;
using Thingie.WPF.Controls.PropertiesEditor.DefaultFactory.Attributes;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.ExchangeRates.Fixer
{
	public class Settings : ISettings
	{
		[Trackable, Editable]
		public string FixerApiKey { get; set; } = "";
	}
}
       