using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QueryStorm.Tools;
using QueryStorm.Apps;
using QueryStorm.Data;

namespace Windy.ExchangeRates.Fixer
{
    public class Implementation
    {
        private readonly Settings settings;

        public Implementation(Settings settings)
    	{
            this.settings = settings;
        }
    
        [ExcelFunction(Name = "Windy.ConvertCurrency", Description = "Converts the specified amount of source currency to destination currency using the API at 'exchangeratesapi.io'."), Cached]
        public async Task<double> ConvertCurrency(double amount, string sourceCurrency, string destinationCurrency, DateTime date)
        {
            var rates = await GetExchangeDataAsync(date, destinationCurrency);
            var rate = rates.Single(r => r.Currency == sourceCurrency);
            return amount / (double)rate.Exchange;
        }

        [ExcelFunction(Name = "Windy.GetExchangeData"), Cached]
        public async Task<IEnumerable<Rate>> GetExchangeDataAsync(DateTime date, string baseCurrency = "EUR")
        {
            var requestUri = $"http://api.exchangeratesapi.io/{date.ToString("yyyy-MM-dd")}?base={baseCurrency}&access_key={settings.FixerApiKey}";
            var res = await new HttpClient().GetAsync(requestUri);
            if (res.IsSuccessStatusCode)
            {
            	var response = await res.Content.ReadAsStringAsync();
                var respObj = JObject.Parse(response);
                var success = respObj.GetValue("success").Value<bool>();
                if (success)
                {
                    string ratesStr = respObj.Property("rates").Value.ToString();
                    return JsonConvert
                        .DeserializeObject<IDictionary<string, decimal>>(ratesStr)
                        .Select(kvp => new Rate() { Currency = kvp.Key, Exchange = kvp.Value });
                }
                else
                {
                	throw new Exception("Fixer returned an error message: " + response);
                }
            }
            else
            {
                throw new HttpRequestException("Http request failed with code " + res.StatusCode);
            }
        }

        public class Rate
        {
            public string Currency { get; set; }
            public decimal Exchange { get; set; }
        }
    }
}
